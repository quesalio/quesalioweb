import React, {Component} from 'react';
import gql from 'graphql-tag';
import {graphql, compose} from 'react-apollo';
import * as moment from "moment-timezone";
import AdsManual from './AdsManual';
import Loader from "react-loader-spinner";

const URL_IMAGE_MEANING = "https://quesalio.com/img/emoticons/";

class Draw extends Component {
    constructor(props) {
        super(props);
        moment.locale('es');
        moment.tz('America/Argentina/Buenos_Aires');
        moment.calendarFormat = function (myMoment, now) {
            let diff = myMoment.diff(now, 'days', true);
            let retVal = diff < -7 ? 'sevenDays' :
                diff < -6 ? 'sixDays' :
                    diff < -5 ? 'fiveDays' :
                        diff < -4 ? 'fourDays' :
                            diff < -3 ? 'threeDays' :
                                diff < -2 ? 'twoDays' :
                                    diff < -1 ? 'lastDay' :
                                        diff < 0 ? 'sameDay' : 'sameElse';
            return retVal;
        };
    }

    renderDrawPrimera() {
        let cities = this.props.city.split(',');
        let primeras = [];
        cities.forEach((city) => {
            this.props.getPrimera.primera.map((primera) => {
                if (primera.id_loteria === city)
                    primeras.push(primera);
                return null;
            });
        });
        //return this.props.getPrimera.primera.map((primera) => {
        return primeras.map((primera) => {
            if (primera.numbers.length === 0) return null;
            //comprobation of the entire block
            let existNumber = primera.numbers.filter((number) => {
                if (number.value !== null) {
                    if (number.value.length > 0)
                        return true;
                    else return null;
                }
                return null;
            });
            if (existNumber.length === 0) return null;
            let image = URL_IMAGE_MEANING + primera.meaning_image;
            let lower = primera.numbers.filter((number) => {
                if (number.number <= 10) return number;
                return null;
            });
            let higher = primera.numbers.filter((number) => {
                if (number.number > 10) return number;
                return null;
            });
            let numbers = lower.map((number, index) => {
                let high = higher[index];
                let backgroundNumber = index === 0 ? 'fondo_posicion1' : 'fondo_posicion';
                let backgroundValue = index === 0 ? 'fondo_posicion' : '';
                return (
                    <tr key={index}>
                        <td className={backgroundNumber}>
                            {number.number}
                        </td>
                        <td className={backgroundValue}>
                            {number.value}
                        </td>
                        <td className="fondo_posicion">
                            {high.number}
                        </td>
                        <td>
                            {high.value}
                        </td>
                    </tr>
                );
            });
            let random = Math.floor(Math.random() * 10) + 1;
            let classNameBackground = "sigbg" + random;
            return (
                <div className="col s12 m6 l3" key={primera.id}>
                    <div className="fondotablero z-depth-2">
                        <img className="rr" src="img/dias.png" alt=""/>
                        <h3 className="titulotablero">LA PRIMERA 11:30</h3>
                        <h2 className="negrita">{primera.city}</h2>
                        <p className="center">
                            <img className={classNameBackground} src={image} alt=""/>
                        </p>

                        <p className="sig truncate">{primera.meaning}</p>
                        <p className="significado">{primera.meaning_number}</p>

                        <table className="centered">
                            <tbody className="contenido-tablero">
                            {numbers}
                            </tbody>
                        </table>
                    </div>
                </div>
            )
        });
    }

    renderDrawMatutina() {
        let cities = this.props.city.split(',');
        let matutinas = [];
        cities.forEach((city) => {
            this.props.getMatutina.matutina.map((matutina) => {
                if (matutina.id_loteria === city)
                    matutinas.push(matutina);
                return null;
            });
        });
        //return this.props.getMatutina.matutina.map((matutina) => {
        return matutinas.map((matutina) => {
            if (matutina.numbers.length === 0) return null;
            //comprobation of the entire block
            let existNumber = matutina.numbers.filter((number) => {
                if (number.value !== null) {
                    if (number.value.length > 0)
                        return true;
                    else return null;
                }
                return null;
            });
            if (existNumber.length === 0) return null;
            let image = URL_IMAGE_MEANING + matutina.meaning_image;
            let lower = matutina.numbers.filter((number) => {
                if (number.number <= 10) return number;
                return null;
            });
            let higher = matutina.numbers.filter((number) => {
                if (number.number > 10) return number;
                return null;
            });
            let numbers = lower.map((number, index) => {
                let high = higher[index];
                let backgroundNumber = index === 0 ? 'fondo_posicion1' : 'fondo_posicion';
                let backgroundValue = index === 0 ? 'fondo_posicion' : '';
                return (
                    <tr key={index}>
                        <td className={backgroundNumber}>
                            {number.number}
                        </td>
                        <td className={backgroundValue}>
                            {number.value}
                        </td>
                        <td className="fondo_posicion">
                            {high.number}
                        </td>
                        <td>
                            {high.value}
                        </td>
                    </tr>
                );
            });
            let random = Math.floor(Math.random() * 10) + 1;
            let classNameBackground = "sigbg" + random;
            return (
                <div className="col s12 m6 l3" key={matutina.id}>
                    <div className="fondotablero z-depth-2">
                        <img className="rr" src="img/dias.png" alt=""/>
                        <h3 className="titulotablero">MATUTINA 14:00</h3>
                        <h2 className="negrita">{matutina.city}</h2>
                        <p className="center">
                            <img className={classNameBackground} src={image} alt=""/>
                        </p>

                        <p className="sig truncate">{matutina.meaning}</p>
                        <p className="significado">{matutina.meaning_number}</p>

                        <table className="centered">
                            <tbody className="contenido-tablero">
                            {numbers}
                            </tbody>
                        </table>
                    </div>
                </div>
            )
        });
    }

    renderDrawVespertina() {
        let cities = this.props.city.split(',');
        let vespertinas = [];
        cities.forEach((city) => {
            this.props.getVespertina.vespertina.map((vespertina) => {
                if (vespertina.id_loteria === city)
                    vespertinas.push(vespertina);
                return null;
            });
        });
        //return this.props.getVespertina.vespertina.map((vespertina) => {
        return vespertinas.map((vespertina) => {
            if (vespertina.numbers.length === 0) return null;
            //comprobation of the entire block
            let existNumber = vespertina.numbers.filter((number) => {
                if (number.value !== null) {
                    if (number.value.length > 0)
                        return true;
                    else return null;
                }
                return null;
            });
            if (existNumber.length === 0) return null;
            let image = URL_IMAGE_MEANING + vespertina.meaning_image;
            let lower = vespertina.numbers.filter((number) => {
                if (number.number <= 10) return number;
                return null;
            });
            let higher = vespertina.numbers.filter((number) => {
                if (number.number > 10) return number;
                return null;
            });
            let numbers = lower.map((number, index) => {
                let high = higher[index];
                let backgroundNumber = index === 0 ? 'fondo_posicion1' : 'fondo_posicion';
                let backgroundValue = index === 0 ? 'fondo_posicion' : '';
                return (
                    <tr key={index}>
                        <td className={backgroundNumber}>
                            {number.number}
                        </td>
                        <td className={backgroundValue}>
                            {number.value}
                        </td>
                        <td className="fondo_posicion">
                            {high.number}
                        </td>
                        <td>
                            {high.value}
                        </td>
                    </tr>
                );
            });
            let random = Math.floor(Math.random() * 10) + 1;
            let classNameBackground = "sigbg" + random;
            return (
                <div className="col s12 m6 l3" key={vespertina.id}>
                    <div className="fondotablero z-depth-2">
                        <img className="rr" src="img/tardes.png" alt=""/>
                        <h3 className="titulotablero">VESPERTINA 17:30</h3>
                        <h2 className="negrita">{vespertina.city}</h2>
                        <p className="center">
                            <img className={classNameBackground} src={image} alt=""/>
                        </p>

                        <p className="sig truncate">{vespertina.meaning}</p>
                        <p className="significado">{vespertina.meaning_number}</p>

                        <table className="centered">
                            <tbody className="contenido-tablero">
                            {numbers}
                            </tbody>
                        </table>
                    </div>
                </div>
            )
        });
    }

    renderDrawNocturna() {
        let cities = this.props.city.split(',');
        let nocturnas= [];
        cities.forEach((city) => {
            this.props.getNocturna.nocturna.map((nocturna) => {
                if (nocturna.id_loteria === city)
                    nocturnas.push(nocturna);
                return null;
            });
        });
        //return this.props.getNocturna.nocturna.map((nocturna) => {
        return nocturnas.map((nocturna) => {
            if (nocturna.numbers.length === 0) return null;
            //comprobation of the entire block
            let existNumber = nocturna.numbers.filter((number) => {
                if (number.value !== null) {
                    if (number.value.length > 0)
                        return true;
                    else return null;
                }
                return null;
            });
            if (existNumber.length === 0) return null;
            let image = URL_IMAGE_MEANING + nocturna.meaning_image;
            let lower = nocturna.numbers.filter((number) => {
                if (number.number <= 10) return number;
                return null;
            });
            let higher = nocturna.numbers.filter((number) => {
                if (number.number > 10) return number;
                return null;
            });
            let numbers = lower.map((number, index) => {
                let high = higher[index];
                let backgroundNumber = index === 0 ? 'fondo_posicion1' : 'fondo_posicion';
                let backgroundValue = index === 0 ? 'fondo_posicion' : '';
                return (
                    <tr key={index}>
                        <td className={backgroundNumber}>
                            {number.number}
                        </td>
                        <td className={backgroundValue}>
                            {number.value}
                        </td>
                        <td className="fondo_posicion">
                            {high.number}
                        </td>
                        <td>
                            {high.value}
                        </td>
                    </tr>
                );
            });
            let random = Math.floor(Math.random() * 10) + 1;
            let classNameBackground = "sigbg" + random;
            return (
                <div className="col s12 m6 l3" key={nocturna.id}>
                    <div className="fondotablero z-depth-2">
                        <img className="rr" src="img/noches.png" alt=""/>
                        <h3 className="titulotablero">NOCTURNA 21:00</h3>
                        <h2 className="negrita">{nocturna.city}</h2>
                        <p className="center">
                            <img className={classNameBackground} src={image} alt=""/>
                        </p>

                        <p className="sig truncate">{nocturna.meaning}</p>
                        <p className="significado">{nocturna.meaning_number}</p>

                        <table className="centered">
                            <tbody className="contenido-tablero">
                            {numbers}
                            </tbody>
                        </table>
                    </div>
                </div>
            )
        });
    }

    renderHeads() {
        let cities = this.props.city.split(',');
        let primeras = [], matutinas = [], vespertinas = [], nocturnas = [];
        cities.forEach((city) => {
            this.props.getPrimera.primera.map((primera) => {
                if (primera.id_loteria === city)
                    primeras.push({'city': primera.city, 'value': primera.value});
                return null;
            });
            this.props.getMatutina.matutina.map((matutina) => {
                if (matutina.id_loteria === city)
                    matutinas.push({'city': matutina.city, 'value': matutina.value});
                return null;
            });
            this.props.getVespertina.vespertina.map((vespertina) => {
                if (vespertina.id_loteria === city)
                    vespertinas.push({'city': vespertina.city, 'value': vespertina.value});
                return null;
            });
            this.props.getNocturna.nocturna.map((nocturna) => {
                if (nocturna.id_loteria === city)
                    nocturnas.push({'city': nocturna.city, 'value': nocturna.value});
                return null;
            });
        });
        return primeras.map((primera, index) => {
            let prim = (typeof primera.value) === 'undefined' ? '----' : primera.value;
            let mat = (typeof matutinas[index] === 'undefined') ? '----' : matutinas[index].value;
            let ves = (typeof vespertinas[index] === 'undefined') ? '----' : vespertinas[index].value;
            let noc = (typeof nocturnas[index] === 'undefined') ? '----' : nocturnas[index].value;

            return (
                <div key={index}>
                    <div className="col s4 m4"><h2 className="truncate b">{primera.city}</h2></div>
                    <div className="col s2 m2"><p className="c">{prim}</p></div>
                    <div className="col s2 m2"><p className="c">{mat}</p></div>
                    <div className="col s2 m2"><p className="c">{ves}</p></div>
                    <div className="col s2 m2"><p className="c">{noc}</p></div>
                </div>
            );
        })
    }

    renderLoading() {
        return (
            <div className="center">
                <Loader
                    type="ThreeDots"
                    color="#fce918"
                    height="50"
                    width="50"
                />
            </div>
        );
    }

    render() {
        if (this.props.getPrimera.loading) return this.renderLoading();
        if (this.props.getMatutina.loading) return null;
        if (this.props.getVespertina.loading) return null;
        if (this.props.getNocturna.loading) return null;

        let letter = moment(this.props.date).calendar(null, {
            sameDay: '[Hoy]',
            lastDay: '[Ayer]',
            twoDays: '[Antes de Ayer]',
            threeDays: '[Hace 3 días]',
            fourDays: '[Hace 4 días]',
            fiveDays: '[Hace 5 días]',
            sixDays: '[Hace 6 días]',
            sameElse: 'LT'
        });
        return (
            <div>
                <div className="z-depth-2 fondotablero1">
                    <div className="col s12 m12 l4 center padf0">
                        <AdsManual/>
                    </div>
                    <div className="col s12 m12 l8 center padf0">
                        <div className="col s4 m4">
                            <h2 className="a truncate white-text">QUINIELA<br/>de {letter ? letter : ''}
                            </h2>
                        </div>
                        <div className="col s2 m2"><p className="a white-text">PRIM<br/>11:30</p></div>
                        <div className="col s2 m2"><p className="a white-text">MATU<br/>14:00</p></div>
                        <div className="col s2 m2"><p className="a white-text">VESP<br/>17:30</p></div>
                        <div className="col s2 m2"><p className="a white-text">NOCT<br/>21:00</p></div>

                        {this.renderHeads()}
                    </div>
                </div>

                <div className="col s12">
                    <div id="tableros">
                        <div className="row center">
                            {this.renderDrawPrimera()}
                        </div>
                        <div className="row center">
                            {this.renderDrawMatutina()}
                        </div>
                        <div className="row center">
                            {this.renderDrawVespertina()}
                        </div>
                        <div className="row center">
                            {this.renderDrawNocturna()}
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

const queries = {
    getPrimera: gql`
            query getPrimera($ids:String,$date:String){
              primera(ids:$ids,date:$date){
                id
                city
                value
                id_loteria
                id_draw
                date
                numbers{
                  number
                  value
                }
                meaning
                meaning_number
                meaning_image
              }
            }
        `,
    getMatutina: gql`
            query getMatutina($ids:String,$date:String){
              matutina(ids:$ids,date:$date){
                id
                city
                value
                id_loteria
                id_draw
                date
                numbers{
                  number
                  value
                }
                meaning
                meaning_number
                meaning_image
              }
            }
        `,
    getVespertina: gql`
            query getVespertina($ids:String,$date:String){
              vespertina(ids:$ids,date:$date){
                id
                city
                value
                id_loteria
                id_draw
                date
                numbers{
                  number
                  value
                }
                meaning
                meaning_number
                meaning_image
              }
            }
        `,
    getNocturna: gql`
            query getNocturna($ids:String,$date:String){
              nocturna(ids:$ids,date:$date){
                id
                city
                value
                id_loteria
                id_draw
                date
                numbers{
                  number
                  value
                }
                meaning
                meaning_number
                meaning_image
              }
            }
        `
};

export default compose(
    graphql(queries.getPrimera, {
        name: 'getPrimera',
        options: (props) => ({variables: {ids: props.city, date: props.date}})
    }),
    graphql(queries.getMatutina, {
        name: 'getMatutina',
        options: (props) => ({variables: {ids: props.city, date: props.date}})
    }),
    graphql(queries.getVespertina, {
        name: 'getVespertina',
        options: (props) => ({variables: {ids: props.city, date: props.date}})
    }),
    graphql(queries.getNocturna, {
        name: 'getNocturna',
        options: (props) => ({variables: {ids: props.city, date: props.date}})
    })
)(Draw);