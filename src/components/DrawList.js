import React, {Component} from 'react';
import gql from 'graphql-tag';
import {graphql, compose} from 'react-apollo';
//import moment from 'moment';
import * as moment from "moment-timezone";
import Draw from './Draw';
import {Input} from 'react-materialize';

let cities = ['Buenos Aires',
    'Entre Rios',
    'Mendoza',
    'Cordoba',
    'Corrientes',
    'Chaco',
    'Santiago',
    'Neuquen',
    'San Luis',
    'Salta',
    'Jujuy',
    'Tucuman',
    'Chubut',
    'Formosa',
    'Misiones',
    'Catamarca',
    'San Juan',
    'La Rioja'];
let dates = [];
let city_indexes = [];
let current_date = "";

class DrawList extends Component {
    constructor(props) {
        super(props);
        moment.locale('es');
        moment.tz('America/Argentina/Buenos_Aires');
        moment.calendarFormat = function (myMoment, now) {
            let diff = myMoment.diff(now, 'days', true);
            let retVal = diff < -7 ? 'sevenDays' :
                diff < -6 ? 'sixDays' :
                    diff < -5 ? 'fiveDays' :
                        diff < -4 ? 'fourDays' :
                            diff < -3 ? 'threeDays' :
                                diff < -2 ? 'twoDays' :
                                    diff < -1 ? 'lastDay' :
                                        diff < 0 ? 'sameDay' : 'sameElse';
            return retVal;
        };
        let day = moment().day();
        city_indexes = [];
        switch (day) {
            default:
            case 1:
            case 2:
            case 4:
                city_indexes.push("25,24,23,38,28");
                break;
            case 3:
                city_indexes.push("25,24,23,38,28");
                break;
            case 5:
                city_indexes.push("25,24,23,38,48,28");
                break;
            case 6:
            case 0:
                city_indexes.push("25,24,23,38,53,28");
                break;
        }
        switch (day) {
            default:
            case 1:
            case 3:
            case 4:
            case 5:
                city_indexes.push("25,24,38,39");
                break;
            case 2:
            case 6:
            case 0:
                city_indexes.push("25,24,38,39,23");
                break;
        }
        city_indexes.push("25,24,53");
        switch (day) {
            default:
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
            case 0:
                city_indexes.push("25,24,28");
                break;
            case 5:
                city_indexes.push("25,24,28,48");
                break;
        }
        city_indexes.push("25,24,42");
        city_indexes.push("25,24,52");
        city_indexes.push("25,24,48");
        city_indexes.push("25,24,41");
        city_indexes.push("25,24,49,53,28");
        city_indexes.push("25,24,51,50,55");
        city_indexes.push("25,24,50");
        city_indexes.push("25,24,55");
        city_indexes.push("25,24,56");
        city_indexes.push("25,24,59");
        city_indexes.push("25,24,38,60");
        city_indexes.push("25,24,61");
        city_indexes.push("25,24,62");
        city_indexes.push("25,24,63");


        dates = [];
        for (let i = 0; i <= 4; i++) {
            moment.locale('es');
            moment.tz('America/Argentina/Buenos_Aires');
            let iterate = moment().subtract(i, 'days');
            let value = iterate.format('YYYY-MM-DD');
            let label = iterate.format('LL');
            dates.push({'label': label, 'value': value})
        }
        this.getDate = this.getDate.bind(this);
        this.getCity = this.getCity.bind(this);
        this.state = {city: city_indexes[0]};
    }

    renderDate() {
        return dates.map((date => {
            let momentDate = moment(date.value);
            let day = momentDate.format('dddd');
            day = day.charAt(0).toUpperCase() + day.slice(1) + ", ";
            return <option value={date.value} key={date.value}>{day}{date.label}</option>;
        }));
    }

    renderCity() {
        return cities.map((city, index) => {
            return (<option value={city_indexes[index]} key={index}>{city}</option>);
        });
    }

    getCity(event) {
        event.preventDefault();
        let city = event.target.value;
        this.setState({city: city});
    }

    getDate(event) {
        let date = event.target.value;
        let letter = moment(date).calendar(null, {
            sameDay: '[Hoy]',
            lastDay: '[Ayer]',
            twoDays: '[Antes de Ayer]',
            threeDays: '[Hace 3 días]',
            fourDays: '[Hace 4 días]',
            fiveDays: '[Hace 5 días]',
            sixDays: '[Hace 6 días]',
            sameElse: 'LT'
        });
        this.setState({date: date, letter: letter});
    }

    render() {
        if (this.props.data.loading) return (<div>Cargando</div>);
        current_date = this.props.data.configuration.date;
        let current_letter = moment(current_date);
        current_letter = current_letter.calendar(null, {
            sameDay: '[Hoy]',
            lastDay: '[Ayer]',
            twoDays: '[Antes de Ayer]',
            threeDays: '[Hace 3 días]',
            fourDays: '[Hace 4 días]',
            fiveDays: '[Hace 5 días]',
            sixDays: '[Hace 6 días]',
            sameElse: 'LT'
        });
        //let cd = current_date.replace('T00:00:00.000Z', '');
        let cd = moment(current_date, "YYYY-MM-DD HH:mm Z");
        cd = cd.utc().format('YYYY-MM-DD');
        return (
            <div>
                <div className="col s12 m6 center">
                    <span className="cuando">Sorteo de {this.state.letter ? this.state.letter : current_letter}</span>
                    <Input s={12} type='select' ref="dateSelector" onChange={this.getDate}
                           defaultValue={this.state.date ? this.state.date : cd}>
                        {this.renderDate()}
                    </Input>
                </div>
                <div className="col s12 m6 center"><span className="cuando">Localidad</span>
                    <Input s={12} type='select' defaultValue="" onChange={this.getCity}>
                        {this.renderCity()}
                    </Input>
                </div>
                <Draw city={this.state.city} date={this.state.date ? this.state.date : current_date}/>
            </div>
        );
    };
}

const query = gql`
    {
      configuration{
        date
      }
    }
`;

export default compose(graphql(query))(DrawList);