import React, {Component} from 'react';
import gql from 'graphql-tag';
import {graphql} from 'react-apollo';

class WeeklyList extends Component {
    renderWeekly() {
        return this.props.data.weekly.map((weekly,index) => {
            return (
                <div key={index}>
                    <p className="dscard">Válido hasta el {new Intl.DateTimeFormat('es-ES', {
                        year: 'numeric',
                        month: 'long',
                        day: '2-digit'
                    }).format(new Date(weekly.date))}</p>
                    <p className="ds1">¡COMO TROMPADA!</p>
                    <p className="ds2">
                        {weekly.number1}
                        <img width="40px" src="img/dontstop2.png" alt=""/>
                        {weekly.number2}
                    </p>
                    <div>
                        <div className="col s3 m3 favo2 dsvio">
                            <img width="40px" src="/img/evitapapi.png" alt=""/>
                            <p className=" favorito">{weekly.number3}</p>
                        </div>

                        <div className="col s3 m3 favo2 dspink">
                            <img width="40px" src="/img/evitapapi2.png" alt=""/>
                            <p className="favorito">{weekly.number4}</p>
                        </div>

                        <div className="col s3 m3 favo2 dsvio">
                            <img width="40px" src="/img/evitapapi.png" alt=""/>
                            <p className="favorito">{weekly.number5}</p>
                        </div>

                        <div className="col s3 m3 favo2 dspink">
                            <img width="40px" src="/img/evitapapi2.png" alt=""/>
                            <p className="favorito">{weekly.number6}</p>
                        </div>
                    </div>
                </div>
            );
        });
    }

    render() {
        if (this.props.data.loading) return (<div>Cargando</div>);
        return (
            <div className="center">
                <div className="col s12 m6 l5">
                    <div className="z-depth-2 fondotablero esquina efe">
                        <p className="tit p1">
                            <img className="icontit" src="img/datos_semanales.png" alt=""/>
                            DATOS SEMANALES
                        </p>
                        {this.renderWeekly()}
                    </div>
                </div>
            </div>
        )
    };
}

const query = gql`
    {
      weekly{
        number1
        number2
        number3
        number4
        number5
        number6
        date
      }
    }
`;

export default graphql(query)(WeeklyList);