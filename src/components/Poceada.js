import React, {Component} from 'react';
import gql from 'graphql-tag';
import {graphql} from 'react-apollo';
import PoceadaDetail from './PoceadaDetail';
import moment from 'moment';
import 'moment/locale/es';
import Navigation from "./Navigation";
import Footer from "./Footer";
import HeaderDraws from './HeaderDraws';
import Loader from "react-loader-spinner";
import {Input} from "react-materialize";
import {initAnalytics} from 'react-with-analytics';
import {Constants} from ".././Constants";
import {trackPage} from 'react-with-analytics';

initAnalytics(Constants.ANALYTICS_ID);

class Poceada extends Component {
    constructor(props) {
        super(props);
        this.renderDetail = this.renderDetail.bind(this);
        moment.locale('es');
        moment.calendarFormat = function (myMoment, now) {
            let diff = myMoment.diff(now, 'days', true);
            let retVal = diff < -6 ? 'sevenDays' :
                diff < -5 ? 'sixDays' :
                    diff < -4 ? 'fiveDays' :
                        diff < -3 ? 'fourDays' :
                            diff < -2 ? 'threeDays' :
                                diff < -1 ? 'twoDays' :
                                    diff < 0 ? 'lastDay' :
                                        diff < 1 ? 'sameDay' : 'sameElse';
            return retVal;
        };
        this.state = {'poceada': null, 'letter': ''}
    }

    renderDate() {
        return this.props.getPoceada.poceada_limit.map((poceada, index) => {
            let date = moment(poceada.date, "YYYY-MM-DD");
            let day = date.format('dddd');
            day = day.charAt(0).toUpperCase() + day.slice(1) + ", ";
            return (
                <option key={index} value={index}>
                    {day}{date.format('LL')}
                </option>
            );
        });
    }

    renderDetail(event) {
        let index = parseInt(event.target.value, 10);
        let poceada = this.props.getPoceada.poceada_limit[index];
        let letter = moment(poceada.date).calendar(null, {
            sameDay: '[Hoy]',
            lastDay: '[Ayer]',
            twoDays: '[Antes de Ayer]',
            threeDays: '[Hace 3 días]',
            fourDays: '[Hace 4 días]',
            fiveDays: '[Hace 5 días]',
            sixDays: '[Hace 6 días]',
            sameElse: 'LT'
        });
        this.setState({'poceada': poceada, 'letter': letter});
    }

    renderLoading() {
        return (
            <div className="center">
                <Loader
                    type="ThreeDots"
                    color="#fce918"
                    height="50"
                    width="50"
                />
            </div>
        );
    }

    render() {
        trackPage('/poceada');
        if (this.props.getPoceada.loading) return this.renderLoading();
        let current = this.props.getPoceada.poceada_limit[0];
        let current_letter = moment(current.date);
        current_letter = current_letter.calendar(null, {
            sameDay: '[Hoy]',
            lastDay: '[Ayer]',
            twoDays: '[Antes de Ayer]',
            threeDays: '[Hace 3 días]',
            fourDays: '[Hace 4 días]',
            fiveDays: '[Hace 5 días]',
            sixDays: '[Hace 6 días]',
            sameElse: 'LT'
        });
        return (
            <div>
                <Navigation/>
                <HeaderDraws/>
                <div className="container">
                    <div className="row">
                        <div id="pozos" className="section">
                            <div className="row center">
                                <div className="col s12 z-depth-2 pozos">
                                    <span
                                        className="cuando">{this.state.letter ? this.state.letter : current_letter}</span>
                                    <Input s={12} type='select' defaultValue="" onChange={this.renderDetail}
                                           className="select-wrapper">
                                        {this.renderDate()}
                                    </Input>
                                    <PoceadaDetail poceada={this.state.poceada ? this.state.poceada : current}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    };
}

const query = gql`
    query getPoceadaLimit($limit:Int){
        poceada_limit(limit:$limit){
            date
            draw_number
            numbers
            prizes{
                hits
                winners
                prize
            }
        }
    }
`;

export default graphql(query, {
    name: 'getPoceada',
    options: (props) => ({variables: {limit: 5}})
})(Poceada);