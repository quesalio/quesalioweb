import React, {Component} from 'react';
import gql from 'graphql-tag';
import {graphql} from 'react-apollo';

const URL_IMAGES = "https://api.quesalio.com/";

class EphemeridesList extends Component {
    renderEphemerides() {
        return this.props.data.ephemerides.map((ephemeride,index) => {
            return (
                <div className="col s6 m3" key={index}>
                    <div className="card hoverable indigo darken-4">
                        <div className="card-image">
                            <span className="nroe">{ephemeride.number}</span>
                            <img src={URL_IMAGES + ephemeride.image} alt={ephemeride.description} width="140px"
                                 height="135px"/>
                        </div>
                        <div className="card-content">
                            <p className="cardefe">{ephemeride.description}</p>
                        </div>
                    </div>
                </div>

            );
        });
    }

    render() {
        if (this.props.data.loading) return (<div>Cargando</div>);
        return (
            <div className="col s12 m8">
                <div className="z-depth-2 fondotablero efe">
                    <p className="tit p1">
                        <img className="icontit" src="img/efemerides.png" alt=""/>EFEMERIDES
                        DE HOY
                    </p>
                    <div className="row">
                        {this.renderEphemerides()}
                    </div>
                </div>
            </div>
        )
    };
}

const query = gql`
    {
      ephemerides{
        number
        image
        description
      }
    }
`;

export default graphql(query)(EphemeridesList);