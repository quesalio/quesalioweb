import React, {Component} from 'react';
import AdSense from 'react-adsense';

class AdsAutomatico extends Component {
    render() {
        return (
            <AdSense.Google
                client='ca-pub-2986788256227274'
                slot='2542701946'
                style={{display: 'block'}}
                format='auto'
                responsive='true'/>
        );
    };
}

export default AdsAutomatico;