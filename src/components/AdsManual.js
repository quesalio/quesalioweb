import React, {Component} from 'react';
import AdSense from 'react-adsense';

class AdsManual extends Component {
    render() {
        return (
            <AdSense.Google
                client='ca-pub-2986788256227274'
                slot='1179209144'
                style={{display: 'block'}}
                layout='in-article'
                format='fluid'
                responsive='true'/>
        );
    };
}

export default AdsManual;