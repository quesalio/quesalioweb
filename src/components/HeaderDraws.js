import React, {Component} from 'react';
import {compose, graphql} from "react-apollo/index";
import gql from "graphql-tag";

class HeaderDraws extends Component {
    render() {
        if (this.props.getPoceada.loading) return null;
        if (this.props.getPoceadaPlus.loading) return null;
        if (this.props.getQuini.loading) return null;
        if (this.props.getLoto.loading) return null;
        let formatter = new Intl.NumberFormat('de-DE', {
            minimumFractionDigits: 2,
        });
        return (
            <div className="container">
                <div className="row">
                    <div className="col s12">
                        <div id="pozos" className="section section_header">
                            <div className="row center">
                                <div className="col s12 z-depth-2 pozos">
                                    <p className="pozotitulo">POZO ESTIMADO PRÓXIMO SORTEO</p>
                                    <div className="col s6 m6 l3 pozo">
                                        <a href="/poceada" className="tooltipped" data-position="top" data-delay="50"
                                           data-tooltip="Sortea todas las noches!">
                                            <div className="100">
                                                <div className="waves-effect waves-teal poceada">
                                                    <div className="titjuegos">POCEADA</div>
                                                    <p className="mp0">VACANTE ${
                                                        this.props.getPoceada.poceada[0].prizes.map((prize) => {
                                                            if (prize.hits === '8' && prize.winners === '0') {
                                                                return formatter.format(prize.prize);
                                                            }
                                                            return null;
                                                        })
                                                    }</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div className="col s6 m6 l3 pozo">
                                        <a href="/plus" className="tooltipped" data-position="top" data-delay="10"
                                           data-tooltip="Sortea todas las noches!">
                                            <div className="100">
                                                <div className="waves-effect waves-teal plus">
                                                    <div className="titjuegos">PLUS</div>
                                                    <p className="mp0">VACANTE ${
                                                        this.props.getPoceadaPlus.poceada_plus[0].prizes.map((prize) => {
                                                            if (prize.hits === '8' && prize.winners === '0') {
                                                                return formatter.format(prize.prize);
                                                            }
                                                            return null;
                                                        })
                                                    }</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div className="col s6 m6 l3 pozo">
                                        <a href="/quini" className="tooltipped" data-position="top" data-delay="10"
                                           data-tooltip="Sortea Miércoles y Domingos!">
                                            <div className="100">
                                                <div className="waves-effect waves-teal quini">
                                                    <div className="titjuegos quini_header">QUINI 6</div>
                                                    <p className="mp0">VACANTE
                                                        ${formatter.format(this.props.getQuini.quini6[0].prize_next_move)}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div className="col s6 m6 l3 pozo">
                                        <a href="/loto" className="tooltipped" data-position="top" data-delay="10"
                                           data-tooltip="Sortea Miércoles y Domingos!">
                                            <div className="100">
                                                <div className="waves-effect waves-teal loto">
                                                    <div className="titjuegos">LOTO</div>
                                                    <p className="mp0">VACANTE
                                                        ${formatter.format(this.props.getLoto.loto[0].prize_next_move)}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}

const queries = {
    getPoceada: gql`
    {
        poceada{
            prizes{
              hits
              winners
              prize
            }
        }
    }`,
    getPoceadaPlus: gql`
    {
        poceada_plus{
            prizes{
              hits
              winners
              prize
            }
        }
    }`,
    getQuini: gql`
    {
        quini6{
            prize_next_move
        }
    }`,
    getLoto: gql`
    {
        loto{
            prize_next_move
        }
    }`
};
export default compose(
    graphql(queries.getPoceada, {
        name: 'getPoceada'
    }),
    graphql(queries.getPoceadaPlus, {
        name: 'getPoceadaPlus'
    }),
    graphql(queries.getQuini, {
        name: 'getQuini'
    }),
    graphql(queries.getLoto, {
        name: 'getLoto'
    })
)(HeaderDraws);