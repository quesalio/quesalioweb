import React, {Component} from 'react';
import gql from 'graphql-tag';
import {graphql} from 'react-apollo';
import moment from 'moment';
import 'moment/locale/es';
import Navigation from "./Navigation";
import Footer from "./Footer";
import Loader from "react-loader-spinner";
import {Input} from "react-materialize";
import HeaderDraws from './HeaderDraws';
import LotoDetail from "./LotoDetail";
import { initAnalytics } from 'react-with-analytics';
import {Constants} from ".././Constants";
import { trackPage} from 'react-with-analytics';
initAnalytics(Constants.ANALYTICS_ID);

class Loto extends Component {
    constructor(props) {
        super(props);
        this.renderDetail = this.renderDetail.bind(this);
        moment.locale('es');
        moment.calendarFormat = function (myMoment, now) {
            let diff = myMoment.diff(now, 'days', true);
            let retVal = diff < -6 ? 'sevenDays' :
                diff < -5 ? 'sixDays' :
                    diff < -4 ? 'fiveDays' :
                        diff < -3 ? 'fourDays' :
                            diff < -2 ? 'threeDays' :
                                diff < -1 ? 'twoDays' :
                                    diff < 0 ? 'lastDay' :
                                        diff < 1 ? 'sameDay' : 'sameElse';
            return retVal;
        };
        this.state = {loto: null, letter: ''}
    }

    renderDate() {
        return this.props.getLoto.loto_limit.map((loto, index) => {
            let date = moment(loto.date, "YYYY-MM-DD");
            let day = date.format('dddd');
            day = day.charAt(0).toUpperCase() + day.slice(1) + ", ";
            return (
                <option key={index} value={index}>
                    {day}{date.format('LL')}
                </option>
            );
        });
    }

    renderDetail(event) {
        let index = parseInt(event.target.value, 10);
        let loto = this.props.getLoto.loto_limit[index];
        let letter = moment(loto.date).calendar(null, {
            sameDay: '[Hoy]',
            lastDay: '[Ayer]',
            twoDays: '[Antes de Ayer]',
            threeDays: '[Hace 3 días]',
            fourDays: '[Hace 4 días]',
            fiveDays: '[Hace 5 días]',
            sixDays: '[Hace 6 días]',
            sameElse: 'LT'
        });
        this.setState({loto: loto, letter: letter});
    }

    renderLoading() {
        return (
            <div className="center">
                <Loader
                    type="ThreeDots"
                    color="#fce918"
                    height="50"
                    width="50"
                />
            </div>
        );
    }

    render() {
        trackPage('/loto');
        if (this.props.getLoto.loading) return this.renderLoading();
        let current = this.props.getLoto.loto_limit[0];
        let current_letter = moment(current.date);
        current_letter = current_letter.calendar(null, {
            sameDay: '[Hoy]',
            lastDay: '[Ayer]',
            twoDays: '[Antes de Ayer]',
            threeDays: '[Hace 3 días]',
            fourDays: '[Hace 4 días]',
            fiveDays: '[Hace 5 días]',
            sixDays: '[Hace 6 días]',
            sameElse: 'LT'
        });
        return (
            <div>
                <Navigation/>
                <HeaderDraws/>
                <div className="container">
                    <div className="row">
                        <div id="pozos" className="section">
                            <div className="row center">
                                <div className="col s12 z-depth-2 pozos">
                                    <span
                                        className="cuando">{this.state.letter ? this.state.letter : current_letter}</span>
                                    <Input s={12} type='select' defaultValue="" onChange={this.renderDetail}
                                           className="select-wrapper">
                                        {this.renderDate()}
                                    </Input>
                                    <LotoDetail loto={this.state.loto ? this.state.loto : current}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    };
}

const query = gql`
    query getLoto($limit:Int){
      loto_limit(limit:$limit){
        date
        draw_number
        tradicional_numbers
        jackpot1_tradicional
        jackpot2_tradicional
        tradicional_prizes{
          label
          winners
          prize
        }
        desquite_numbers
        jackpot1_desquite
        jackpot2_desquite
        desquite_prizes{
          label
          winners
          prize
        }
        sale_sale_numbers
        jackpot1_sale_sale
        jackpot2_sale_sale
        jackpot3_sale_sale
        jackpot4_sale_sale
        sale_sale_prizes{
          winners
          prize
        }
        sale_sale_duplicador
        prize_next_move
      }
    }
`;

export default graphql(query, {
    name: 'getLoto',
    options: (props) => ({variables: {limit: 5}})
})(Loto);