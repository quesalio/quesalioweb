import React, {Component} from 'react';
import gql from 'graphql-tag';
import {graphql} from 'react-apollo';

class DollarList extends Component {
    render() {
        if (this.props.data.loading) return (<div>Cargando</div>);
        let dollar=this.props.data.dollar;
        return (
            <div>
                <div className="col s12 m6 l3">
                    <div className="z-depth-2 fondotablero">
                        <p className="tit"><img className="icontit" src="img/verde.png" alt=""/>DOLAR</p>
                        <div className="col s6">
                            <p>VENTA</p>
                            <p>{dollar.sell}</p>
                        </div>
                        <div className="col s6">
                            <p>COMPRA</p>
                            <p>{dollar.buy}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
}

const query = gql`
    {
      dollar{
        buy
        sell
      }
    }
`;

export default graphql(query)(DollarList);