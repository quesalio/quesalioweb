import React, {Component} from 'react';
import AdsAutomatico from "./AdsAutomatico";


class QuiniDetail extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            message: '',
            number1: '',
            number2: '',
            number3: '',
            number4: '',
            number5: '',
            number6: '',
            actives: [
                '', '', '', '', '',
                '', '', '', '', '',
                '', '', '', '', '',
                '', '', '', '', ''
            ]
        }
    }

    checkValue(event, index) {
        let value = event.target.value;
        switch (index) {
            case 1:
                this.setState({number1: value});
                break;
            case 2:
                this.setState({number2: value});
                break;
            case 3:
                this.setState({number3: value});
                break;
            case 4:
                this.setState({number4: value});
                break;
            case 5:
                this.setState({number5: value});
                break;
            case 6:
                this.setState({number6: value});
                break;
            default:
                break;
        }
    }

    generateMessage(hits) {
        let formatter = new Intl.NumberFormat('de-DE', {
            minimumFractionDigits: 2,
        });
        let message = "";
        switch (hits) {
            default:
            case 0:
                message = "¡Que lástima, ningún acierto!";
                break;
            case 1:
                message = "¡Que lástima, solo 1 acierto!";
                break;
            case 2:
                message = "¡Que lástima, solo 2 aciertos!";
                break;
            case 3:
                message = "¡Que lástima, solo 3 aciertos!";
                break;
            case 4:
                message = "¡Que lástima, solo 4 aciertos!";
                break;
            case 5:
                this.props.quini.tradicional_prizes.map((prize) => {
                    if (prize.hits === '6') {
                        message = `¡Que lástima, te falto 1 más para el pozo de los 6 aciertos $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
            case 6:
                this.props.quini.tradicional_prizes.map((prize) => {
                    if (prize.hits === '6') {
                        message = `¡GANASTE EL POZO DE LOS 6 ACIERTOS $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
        }
        this.setState({message: message});
    }

    handleSubmit() {
        let actives = this.state.actives;
        let empty_actives = actives.forEach((value, index) => {
            actives[index] = '';
        });
        this.setState({actives: empty_actives});
        let values = [this.state.number1, this.state.number2, this.state.number3, this.state.number4, this.state.number5, this.state.number6];
        let hits = 0;
        values.forEach(value => {
            let index = this.props.quini.numbers.indexOf(value);
            if (index >= 0) {
                actives[index] = 'active_ball';
                hits++;
            }
        });
        this.setState({actives: actives});
        this.generateMessage(hits);
    }

    renderComprobation() {
        return (
            <div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number1}
                           onChange={event => this.checkValue(event, 1)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number2}
                           onChange={event => this.checkValue(event, 2)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number3}
                           onChange={event => this.checkValue(event, 3)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number4}
                           onChange={event => this.checkValue(event, 4)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number5}
                           onChange={event => this.checkValue(event, 5)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number6}
                           onChange={event => this.checkValue(event, 6)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
            </div>

        );
    }

    renderDetail() {
        let formatter = new Intl.NumberFormat('de-DE', {
            minimumFractionDigits: 2,
        });
        let quini = this.props.quini;

        let primera_tradicional = quini.tradicional_numbers.map((tradicional, index) => {
            let className = `col s4 quini_tradicional ${this.state.actives[index]}`;
            return <div className={className} key={index}>
                {tradicional}
            </div>
        });
        let winners_primera_tradicional = quini.tradicional_prizes.map((prize, index) => {
            return (
                <div key={index}>
                    <div className="col s4 quini_subtit quini_bgrojo">CON {prize.hits} ACIERTOS</div>
                    <div className="col s4 quini_subtit quini_bgrojo">
                        {prize.winners === '0' ? 'POZO VACANTE' : prize.winners === '1' ? prize.winners + 'GANADOR' : prize.winners + ' GANADORES'}
                    </div>
                    <div className="col s4 quini_subtit quini_bgrojo">Cada uno ${formatter.format(prize.prize)}</div>
                </div>
            );
        });
        let segunda_vuelta = quini.segunda_vuelta_numbers.map((segunda, index) => {
            let className = `col s4 quini_tradicional ${this.state.actives[index]}`;
            return <div className={className} key={index}>
                {segunda}
            </div>
        });
        let winners_segunda_vuelta = quini.segunda_vuelta_prizes.map((prize, index) => {
            return (
                <div key={index}>
                    <div className="col s4 quini_subtit quini_bgverde">CON {prize.hits} ACIERTOS</div>
                    <div className="col s4 quini_subtit quini_bgverde">
                        {prize.winners === '0' ? 'POZO VACANTE' : prize.winners === '1' ? prize.winners + 'GANADOR' : prize.winners + ' GANADORES'}
                    </div>
                    <div className="col s4 quini_subtit quini_bgverde">Cada uno ${formatter.format(prize.prize)}</div>
                </div>
            );
        });

        let revancha = quini.revancha_numbers.map((revancha, index) => {
            let className = `col s4 quini_tradicional ${this.state.actives[index]}`;
            return <div className={className} key={index}>
                {revancha}
            </div>
        });
        let prizes_revancha = quini.revancha_prizes;
        let winners_revancha =
            <div>
                <div className="col s4 quini_subtit quini_bgazul">CON 6 ACIERTOS</div>
                <div className="col s4 quini_subtit quini_bgazul">
                    {prizes_revancha.winners === '0' ? 'POZO VACANTE' : prizes_revancha.winners === '1' ? prizes_revancha.winners + 'GANADOR' : prizes_revancha.winners + ' GANADORES'}
                </div>
                <div className="col s4 quini_subtit quini_bgazul">Cada uno
                    ${formatter.format(prizes_revancha.prize)}</div>
            </div>;
        let siempre_sale = quini.siempre_sale_numbers.map((siempre_sale, index) => {
            let className = `col s4 quini_tradicional ${this.state.actives[index]}`;
            return <div className={className} key={index}>
                {siempre_sale}
            </div>
        });
        let prizes_siempre_sale = quini.siempre_sale_prizes;
        let winners_siempre_sale =
            <div>
                <div className="col s4 quini_subtit quini_bgamarillo">CON 6 ACIERTOS</div>
                <div className="col s4 quini_subtit quini_bgamarillo">
                    {prizes_siempre_sale.winners === '0' ? 'POZO VACANTE' : prizes_siempre_sale.winners === '1' ? prizes_siempre_sale.winners + 'GANADOR' : prizes_siempre_sale.winners + ' GANADORES'}
                </div>
                <div className="col s4 quini_subtit quini_bgamarillo">Cada uno
                    ${formatter.format(prizes_siempre_sale.prize)}</div>
            </div>;
        let winners_super_canasta =
            <div>
                <div className="col s4 quini_subtit quini_bgvioleta">CON {quini.premio_extra_prizes.hits} ACIERTOS</div>
                <div className="col s4 quini_subtit quini_bgvioleta">
                    {quini.premio_extra_prizes.winners === '0' ? 'POZO VACANTE' : quini.premio_extra_prizes.winners === '1' ? quini.premio_extra_prizes.winners + 'GANADOR' : quini.premio_extra_prizes.winners + ' GANADORES'}
                </div>
                <div className="col s4 quini_subtit quini_bgvioleta">Cada uno ${quini.premio_extra_prizes.prize}</div>
            </div>;


        return (
            <div>
                <div className="quini_bg">
                    <div className="col s12">
                        <div>
                            <AdsAutomatico/>
                        </div>
                        <h1 className="q1">QUINI 6</h1>
                        <div className="col s12">
                            <label>Mi jugada:</label>
                            {this.renderComprobation()}
                            <div className="waves-effect waves-teal">
                                <button
                                    onClick={this.handleSubmit}
                                    className="waves-light btn-large indigo darken-4">
                                    Controlar
                                </button>
                            </div>
                        </div>
                        <div className="col s12">
                            <p className="quini_titrojo">TRADICIONAL PRIMER SORTEO</p>
                            <p className="titaci">
                                <span id="aciertos">{this.state.message}</span>
                            </p>
                            {primera_tradicional}
                        </div>
                        <div className="col s12">
                            {winners_primera_tradicional}
                        </div>

                        <div className="col s12">
                            <p className="quini_titverde">TRADICIONAL SEGUNDA VUELTA</p>
                            {segunda_vuelta}
                        </div>
                        <div className="col s12">
                            {winners_segunda_vuelta}
                        </div>

                        <div className="col s12">
                            <p className="quini_titazul">REVANCHA</p>
                            {revancha}
                        </div>
                        <div className="col s12">
                            {winners_revancha}
                        </div>

                        <div className="col s12">
                            <p className="quini_titamarillo">SIEMPRE SALE</p>
                            {siempre_sale}
                        </div>
                        <div className="col s12">
                            {winners_siempre_sale}
                        </div>

                        <div className="col s12 l6">
                            <p className="quini_titvioleta">SUPER CANASTA DE PREMIOS EXTRA</p>
                            <div className="col s12 quini_tradicional">${formatter.format(quini.premio_extra)}</div>
                            {winners_super_canasta}
                        </div>
                        <div className="col s12 l6">
                            <p className="quini_titrojo">POZO ESTIMADO PRÓXIMA JUGADA</p>
                            <div
                                className="col s12 quini_tradicional">${formatter.format(quini.prize_next_move)}
                            </div>
                            <div className="col s12 quini_subtit quini_bgrojo end">¡SUERTE!
                            </div>
                        </div>
                        <p>AVISO: CONTROLE LA JUGADA EN SU AGENCIA OFICIAL AMIGA</p>
                    </div>
                </div>
                <div>
                    <AdsAutomatico/>
                </div>
            </div>
        );
    }

    render() {
        return this.renderDetail();
    };
}

export default QuiniDetail;