import React, {Component} from 'react';
import gql from 'graphql-tag';
import {graphql} from 'react-apollo';
import moment from 'moment';
import 'moment/locale/es';
import Navigation from "./Navigation";
import Footer from "./Footer";
import Loader from "react-loader-spinner";
import {Input} from "react-materialize";
import HeaderDraws from './HeaderDraws';
import QuiniDetail from "./QuiniDetail";
import { initAnalytics } from 'react-with-analytics';
import {Constants} from ".././Constants";
import { trackPage} from 'react-with-analytics';
initAnalytics(Constants.ANALYTICS_ID);

class Quini extends Component {
    constructor(props) {
        super(props);
        this.renderDetail = this.renderDetail.bind(this);
        moment.locale('es');
        moment.calendarFormat = function (myMoment, now) {
            let diff = myMoment.diff(now, 'days', true);
            let retVal = diff < -6 ? 'sevenDays' :
                diff < -5 ? 'sixDays' :
                    diff < -4 ? 'fiveDays' :
                        diff < -3 ? 'fourDays' :
                            diff < -2 ? 'threeDays' :
                                diff < -1 ? 'twoDays' :
                                    diff < 0 ? 'lastDay' :
                                        diff < 1 ? 'sameDay' : 'sameElse';
            return retVal;
        };
        this.state = {quini: null, letter: ''}
    }


    renderDate() {
        return this.props.getQuini.quini6_limit.map((quini, index) => {
            let date = moment(quini.date, "YYYY-MM-DD");
            let day = date.format('dddd');
            day = day.charAt(0).toUpperCase() + day.slice(1) + ", ";
            return (
                <option key={index} value={index}>
                    {day}{date.format('LL')}
                </option>
            );
        });
    }

    renderDetail(event) {
        let index = parseInt(event.target.value, 10);
        let quini = this.props.getQuini.quini6_limit[index];
        let letter = moment(quini.date).calendar(null, {
            sameDay: '[Hoy]',
            lastDay: '[Ayer]',
            twoDays: '[Antes de Ayer]',
            threeDays: '[Hace 3 días]',
            fourDays: '[Hace 4 días]',
            fiveDays: '[Hace 5 días]',
            sixDays: '[Hace 6 días]',
            sameElse: 'LT'
        });
        this.setState({quini: quini, letter: letter});
    }

    renderLoading() {
        return (
            <div className="center">
                <Loader
                    type="ThreeDots"
                    color="#fce918"
                    height="50"
                    width="50"
                />
            </div>
        );
    }

    render() {
        trackPage('/quini');
        if (this.props.getQuini.loading) return this.renderLoading();
        let current = this.props.getQuini.quini6_limit[0];
        let current_letter = moment(current.date);
        current_letter = current_letter.calendar(null, {
            sameDay: '[Hoy]',
            lastDay: '[Ayer]',
            twoDays: '[Antes de Ayer]',
            threeDays: '[Hace 3 días]',
            fourDays: '[Hace 4 días]',
            fiveDays: '[Hace 5 días]',
            sixDays: '[Hace 6 días]',
            sameElse: 'LT'
        });
        return (
            <div>
                <Navigation/>
                <HeaderDraws/>
                <div className="container">
                    <div className="row">
                        <div id="pozos" className="section">
                            <div className="row center">
                                <div className="col s12 z-depth-2 pozos">
                                    <span
                                        className="cuando">{this.state.letter ? this.state.letter : current_letter}</span>
                                    <Input s={12} type='select' defaultValue="" onChange={this.renderDetail}
                                           className="select-wrapper">
                                        {this.renderDate()}
                                    </Input>
                                    <QuiniDetail quini={this.state.quini ? this.state.quini : current}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        )
    };
}

const query = gql`
    query getQuini($limit:Int){
      quini6_limit(limit:$limit){
        date
        draw_number
        tradicional_numbers
        tradicional_prizes{
          hits
          winners
          prize
        }
        segunda_vuelta_numbers
        segunda_vuelta_prizes{
          hits
          winners
          prize
        }
        revancha_numbers
        revancha_prizes{
          winners
          prize
        }
        siempre_sale_numbers
        siempre_sale_prizes{
          winners
          prize
        }
        premio_extra
        premio_extra_prizes{
          hits
          winners
          prize
        }
        prize_next_move
      }
    }
`;

export default graphql(query, {
    name: 'getQuini',
    options: (props) => ({variables: {limit: 5}})
})(Quini);