import React, {Component} from 'react';
import Navigation from './Navigation';
import Footer from './Footer';
import moment from 'moment';
import AdsAutomatico from "./AdsAutomatico";

class HoroscopeSign extends Component {

    constructor(props) {
        super(props);
        this.state = {
            greeting: '',
            date: '',
            sign: '',
            sign_image: '',
            general: '',
            number_one: '',
            number_two: '',
            celebrity1: '',
            image1: '',
            celebrity2: '',
            image2: ''
        };
    }

    componentDidMount() {
        let url_to_fetch = `https://quesalio.com/fetcher_horoscope.php?action=single&sign=${this.props.sign}`;
        fetch(url_to_fetch)
            .then(results => {
                return results.json();
            }).then(data => {
            let date = moment(data.date, "DD/MM/YYYY");
            this.setState({
                date: date.format('LL'),
                sign: data.sign,
                sign_image: 'img/horoscopo_img/' + data.sign + '.png',
                general: data.general,
                number_one: data.number_one,
                number_two: data.number_two,
                celebrity1: data.celebrity_name1,
                celebrity2: data.celebrity_name2,
                image1: data.celebrity_image1,
                image2: data.celebrity_image2
            })
        })
    }

    render() {
        if (this.props.isLoading) return <div>Loading</div>;
        let hour = moment().hour();
        let greeting = "";
        if (hour > 5 && hour < 14) {
            greeting = `<span role="img" aria-label="day">&#x1F31E;;</span> Bueenos dias ${this.state.sign}!`;
        } else if (hour > 13 && hour < 20) {
            greeting = `<span role="img" aria-label="day">&#x1F31E;;</span> Bueenas tardes ${this.state.sign}!`;
        } else if (hour > 19 && hour < 23) {
            greeting = `<span role="img" aria-label="night">&#x1F303;</span> Bueenas noches ${this.state.sign}!`;
        } else if (hour >= 0 && hour < 6) {
            greeting = `<span role="img" aria-label="night">&#x1F303;</span> Bueenas noches ${this.state.sign}!`;
        }
        return (
            <div>
                <Navigation/>
                <div className="container">
                    <div className="col s12 m12 l4 center">
                        <AdsAutomatico/>
                    </div>
                    <div className="row">
                        <div>
                            <div id="pozos" className="section">
                                <div className="z-depth-2 center col s12 l9 violeta2">
                                    <div className="col s12">
                                        <p className="title_date_horoscope">
                                            {this.state.date}
                                        </p>
                                        <img className="bola2" src="img/logo_horoscopo1.png" alt=""/>
                                        <img src={this.state.sign_image} alt=""/>
                                    </div>
                                    <div className="col s12 bghoroscopo">
                                        <p className="title_saludo" dangerouslySetInnerHTML={{__html: greeting}}></p>
                                        <p className="horo_cont">
                                            {this.state.general}
                                        </p>
                                        <p className="title_hor">
                                            <img src="img/star.png" alt=""/>¡JUGATE!</p>
                                        <p className="horo_cont">
                                        <span className="nroastral">
                                            {this.state.number_one}
                                        </span>
                                            <span className="nroastral">
                                            {this.state.number_two}
                                        </span>
                                        </p>
                                        <p className="title_hor">
                                            <img alt="" src="img/star.png"/>FAMOSOS DE {this.state.sign}
                                        </p>
                                        <div className="horo_cont">
                                            <div className="col s6 m3">
                                                <div className="card hoverable card_celebrity">
                                                    <img src={this.state.image1} alt=""/>
                                                    <div className="card-image">
                                                        <p className="nrosoc1">{this.state.celebrity1}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col s6 m3">
                                                <div className="card hoverable card_celebrity">
                                                    <img src={this.state.image2} alt=""/>
                                                    <div className="card-image">
                                                        <p className="nrosoc1">{this.state.celebrity2}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col s12 m12 l4 center">
                                            <AdsAutomatico/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col s12 l3 violeta2">
                                    <div className="z-depth-2 center">
                                        <img className="bola2" src="img/logo_horoscopo1.png" alt=""/>
                                        <p className="classhor">HORÓSCOPO</p>
                                        <p className="classstar">TU ESTRELLA</p>
                                        <div className="col s12 bghoroscopo">
                                            <div className="col s6 l6">
                                                <a href="/aries">
                                                    <img width="90px" src="img/horoscopo_img/aries.png" alt=""/>
                                                    <p className="titlehoroscopo">ARIES</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/tauro">
                                                    <img width="90px" src="img/horoscopo_img/tauro.png" alt=""/>
                                                    <p className="titlehoroscopo">TAURO</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/geminis">
                                                    <img width="90px" src="img/horoscopo_img/geminis.png" alt=""/>
                                                    <p className="titlehoroscopo">GEMINIS</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/cancer">
                                                    <img width="90px" src="img/horoscopo_img/cancer.png" alt=""/>
                                                    <p className="titlehoroscopo">CANCER</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/leo">
                                                    <img width="90px" src="img/horoscopo_img/leo.png" alt=""/>
                                                    <p className="titlehoroscopo">LEO</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/virgo">
                                                    <img width="90px" src="img/horoscopo_img/virgo.png" alt=""/>
                                                    <p className="titlehoroscopo">VIRGO</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/libra">
                                                    <img width="90px" src="img/horoscopo_img/libra.png" alt=""/>
                                                    <p className="titlehoroscopo">LIBRA</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/escorpio">
                                                    <img width="90px" src="img/horoscopo_img/escorpio.png" alt=""/>
                                                    <p className="titlehoroscopo">ESCORPIO</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/sagitario">
                                                    <img width="90px" src="img/horoscopo_img/sagitario.png" alt=""/>
                                                    <p className="titlehoroscopo">SAGITARIO</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/capricornio">
                                                    <img width="90px" src="img/horoscopo_img/capricornio.png" alt=""/>
                                                    <p className="titlehoroscopo">CAPRICORNIO</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="/acuario">
                                                    <img width="90px" src="img/horoscopo_img/acuario.png" alt=""/>
                                                    <p className="titlehoroscopo">ACUARIO</p>
                                                </a>
                                            </div>
                                            <div className="col s6 l6">
                                                <a href="piscis">
                                                    <img width="90px" src="img/horoscopo_img/piscis.png" alt=""/>
                                                    <p className="titlehoroscopo">PISCIS</p>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="col s12 bghoroscopo">
                                            <p className="b1">¡Bajate la APP
                                                <img src="img/horoscopo_img/notificaciones.png" width="21" alt=""/>!
                                            </p>
                                            <p className="b2">y recibí tu Horóscopo Diario!</p>
                                            <a href="https://play.google.com/store/apps/details?id=socialgold.horoscopotuestrella">
                                                <img src="img/horoscopo_img/promo_horoscopo10.png" alt=""/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    };
}

export default HoroscopeSign;