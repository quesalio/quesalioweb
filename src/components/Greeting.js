import React, {Component} from 'react';
import moment from 'moment';

class Greeting extends Component {
    render() {
        let greeting = "";
        let hour = moment().hour();
        if (hour > 5 && hour < 14) {
            greeting = `<div class="elsol"></div>
                        <div class="p1"><h1 class="p2 first">QUINIELA</h1></div>
                        <p>
                        <span class="c1">Bueen dia REY! <span>&#x1F44C;</span></span>
                        </p>`;
        } else if (hour > 13 && hour < 20) {
            greeting = `<div class="elsolnaranja"></div>
                        <div class="p1"><h1 class="p2 first">QUINIELA</h1></div>
                        <p>
                        <span class="c1">Bueenas tardes REY! <span>&#x1F44C;</span></span>
                        </p>`;
        } else if (hour > 19 && hour < 23) {
            greeting = `<div class="lunalunera"></div>
                        <div class="p1"><h1 class="p2 first">QUINIELA</h1></div>
                        <p>
                        <span class="c1">Bueenas noches REY! <span>&#x1F44C;</span></span>
                        </p>`;
        } else if (hour >= 0 && hour < 6) {
            greeting = `<div class="lunalunera"></div>
                        <div class="p1"><h1 class="p2 first">QUINIELA</h1></div>
                        <p>
                        <span class="c1">Bueenas noches REY! <span>&#x1F44C;</span></span>
                        </p>`;
        }
        return (
            <div id="bienvenida">
                <div className="row center">
                    <div className="col s12 z-depth-4 bg_bienvenida2 pad3" dangerouslySetInnerHTML={{__html: greeting}}>                       
                    </div>
                </div>
            </div>
        );
    }
}

export default Greeting;