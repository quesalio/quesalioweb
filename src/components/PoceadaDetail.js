import React, {Component} from 'react';
import AdsAutomatico from "./AdsAutomatico";


class PoceadaDetail extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            message: '',
            number1: '',
            number2: '',
            number3: '',
            number4: '',
            number5: '',
            number6: '',
            number7: '',
            number8: '',
            actives: [
                '', '', '', '', '',
                '', '', '', '', '',
                '', '', '', '', '',
                '', '', '', '', ''
            ]
        }
    }

    checkValue(event, index) {
        let value = event.target.value;
        switch (index) {
            case 1:
                this.setState({number1: value});
                break;
            case 2:
                this.setState({number2: value});
                break;
            case 3:
                this.setState({number3: value});
                break;
            case 4:
                this.setState({number4: value});
                break;
            case 5:
                this.setState({number5: value});
                break;
            case 6:
                this.setState({number6: value});
                break;
            case 7:
                this.setState({number7: value});
                break;
            case 8:
                this.setState({number8: value});
                break;
            default:
                break;
        }
    }

    generateMessage(hits) {
        let formatter = new Intl.NumberFormat('de-DE', {
            minimumFractionDigits: 2,
        });
        let message = "";
        switch (hits) {
            default:
            case 0:
                message = "¡Que lástima, ningún acierto!";
                break;
            case 1:
                message = "¡Que lástima, solo 1 acierto!";
                break;
            case 2:
                message = "¡Que lástima, solo 2 aciertos!";
                break;
            case 3:
                message = "¡Que lástima, solo 3 aciertos!";
                break;
            case 4:
                message = "¡Que lástima, solo 4 aciertos!";
                break;
            case 5:
                this.props.poceada.prizes.map((prize) => {
                    if (prize.hits === '6') {
                        message = `¡Que lástima, te falto 1 más para el pozo de los 6 aciertos $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
            case 6:
                this.props.poceada.prizes.map((prize) => {
                    if (prize.hits === '6') {
                        message = `¡GANASTE EL POZO DE LOS 6 ACIERTOS $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
            case 7:
                this.props.poceada.prizes.map((prize) => {
                    if (prize.hits === '7') {
                        message = `¡GANASTE EL POZO DE LOS 7 ACIERTOS $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
            case 8:
                this.props.poceada.prizes.map((prize) => {
                    if (prize.hits === '8') {
                        message = `¡GANASTE EL POZO DE LOS 8 ACIERTOS $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
        }
        this.setState({message: message});
    }

    handleSubmit() {
        let actives = this.state.actives;
        let empty_actives = actives.forEach((value, index) => {
            actives[index] = '';
        });
        this.setState({actives: empty_actives});
        let values = [this.state.number1, this.state.number2, this.state.number3, this.state.number4, this.state.number5, this.state.number6, this.state.number7, this.state.number8];
        let hits = 0;
        values.forEach(value => {
            let index = this.props.poceada.numbers.indexOf(value);
            if (index >= 0) {
                actives[index] = 'active_ball';
                hits++;
            }
        });
        this.setState({actives: actives});
        this.generateMessage(hits);
    }

    renderComprobation() {
        return (
            <div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number1}
                           onChange={event => this.checkValue(event, 1)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number2}
                           onChange={event => this.checkValue(event, 2)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number3}
                           onChange={event => this.checkValue(event, 3)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number4}
                           onChange={event => this.checkValue(event, 4)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number5}
                           onChange={event => this.checkValue(event, 5)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number6}
                           onChange={event => this.checkValue(event, 6)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number7}
                           onChange={event => this.checkValue(event, 7)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number8}
                           onChange={event => this.checkValue(event, 8)}
                           name="numero" width="10" maxLength="2" className="boletapoceada"/>
                </div>
            </div>

        );
    }

    renderDetail() {
        let formatter = new Intl.NumberFormat('de-DE', {
            minimumFractionDigits: 2,
        });
        let poceada = this.props.poceada;
        let numbers = poceada.numbers.map((number, index) => {
            let className = `btn-floating_poceada poceada_numero poc6 ${this.state.actives[index]}`;
            return <div className="col s3" key={index}>
                <p className={className}>
                    {number}
                </p>
            </div>
        });
        let winners = poceada.prizes.map((prize, index) => {
            return (
                <div key={index}>
                    <div className="col s4 quini_subtit poceada_bg">CON {prize.hits} ACIERTOS</div>
                    <div className="col s4 quini_subtit poceada_bg">
                        {prize.winners === '0' ? 'POZO VACANTE' : prize.winners === '1' ? prize.winners + 'GANADOR' : prize.winners + ' GANADORES'}
                    </div>
                    <div className="col s4 quini_subtit poceada_bg">Cada uno ${formatter.format(prize.prize)}</div>
                </div>
            );
        });
        let next_prize = poceada.prizes.map((prize, index) => {
            if (prize.hits === '8' && prize.winners === '0') {
                return <span key={index}>Proximo Pozo ${formatter.format(prize.prize)}</span>
            }
            return null;
        });
        return (
            <div>
                <div className="pbg3">
                    <div className="col s12">
                        <div>
                            <AdsAutomatico/>
                        </div>
                        <h1 className="q2">POCEADA</h1>
                        <div className="col s12">
                            <label>Mi jugada:</label>
                            {this.renderComprobation()}
                            <div className="waves-effect waves-teal">
                                <button
                                    onClick={this.handleSubmit}
                                    className="waves-light btn-large indigo darken-4">
                                    Controlar
                                </button>
                            </div>
                        </div>
                        <div className="col s12">
                            <p className="poceada_tit">POCEADA - NÚMEROS GANADORES</p>
                            <p className="titaci">
                                <span id="aciertos">{this.state.message}</span>
                            </p>
                            {numbers}
                        </div>
                    </div>
                    <div className="col s12 margin2">
                        {winners}
                    </div>
                    <div className="col s12">
                        <p className="poceada_tit">POZO ESTIMADO PRÓXIMA JUGADA</p>
                        <div className="col s12 quini_tradicional">{next_prize}</div>
                        <div className="col s12 suerte">¡SUERTE!</div>
                        <p>AVISO: CONTROLE LA JUGADA EN SU AGENCIA OFICIAL AMIGA</p>
                    </div>
                </div>
                <div>
                    <AdsAutomatico/>
                </div>
            </div>
        );
    }

    render() {
        return this.renderDetail();
    };
}

export default PoceadaDetail;