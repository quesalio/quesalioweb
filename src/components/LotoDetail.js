import React, {Component} from 'react';
import AdsAutomatico from "./AdsAutomatico";


class LotoDetail extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            message: '',
            number1: '',
            number2: '',
            number3: '',
            number4: '',
            number5: '',
            number6: '',
            number7: '',
            number8: '',
            actives: [
                '', '', '', '', '',
                '', '', '', '', '',
                '', '', '', '', '',
                '', '', '', '', ''
            ]
        }
    }

    checkValue(event, index) {
        let value = event.target.value;
        switch (index) {
            case 1:
                this.setState({number1: value});
                break;
            case 2:
                this.setState({number2: value});
                break;
            case 3:
                this.setState({number3: value});
                break;
            case 4:
                this.setState({number4: value});
                break;
            case 5:
                this.setState({number5: value});
                break;
            case 6:
                this.setState({number6: value});
                break;
            case 7:
                this.setState({number7: value});
                break;
            case 8:
                this.setState({number8: value});
                break;
            default:
                break;
        }
    }

    generateMessage(hits) {
        let formatter = new Intl.NumberFormat('de-DE', {
            minimumFractionDigits: 2,
        });
        let message = "";
        switch (hits) {
            default:
            case 0:
                message = "¡Que lástima, ningún acierto!";
                break;
            case 1:
                message = "¡Que lástima, solo 1 acierto!";
                break;
            case 2:
                message = "¡Que lástima, solo 2 aciertos!";
                break;
            case 3:
                message = "¡Que lástima, solo 3 aciertos!";
                break;
            case 4:
                message = "¡Que lástima, solo 4 aciertos!";
                break;
            case 5:
                this.props.poceada.prizes.map((prize) => {
                    if (prize.hits === '6') {
                        message = `¡Que lástima, te falto 1 más para el pozo de los 6 aciertos $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
            case 6:
                this.props.poceada.prizes.map((prize) => {
                    if (prize.hits === '6') {
                        message = `¡GANASTE EL POZO DE LOS 6 ACIERTOS $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
            case 7:
                this.props.poceada.prizes.map((prize) => {
                    if (prize.hits === '7') {
                        message = `¡GANASTE EL POZO DE LOS 7 ACIERTOS $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
            case 8:
                this.props.poceada.prizes.map((prize) => {
                    if (prize.hits === '8') {
                        message = `¡GANASTE EL POZO DE LOS 8 ACIERTOS $${formatter.format(prize.prize)}!`;
                    }
                    return null;
                });
                break;
        }
        this.setState({message: message});
    }

    handleSubmit() {
        let actives = this.state.actives;
        let empty_actives = actives.forEach((value, index) => {
            actives[index] = '';
        });
        this.setState({actives: empty_actives});
        let values = [this.state.number1, this.state.number2, this.state.number3, this.state.number4, this.state.number5, this.state.number6, this.state.number7, this.state.number8];
        let hits = 0;
        values.forEach(value => {
            let index = this.props.poceada.numbers.indexOf(value);
            if (index >= 0) {
                actives[index] = 'active_ball';
                hits++;
            }
        });
        this.setState({actives: actives});
        this.generateMessage(hits);
    }

    renderComprobation() {
        return (
            <div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number1}
                           onChange={event => this.checkValue(event, 1)}
                           name="numero" width="10" maxLength="2" className="boletaloto"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number2}
                           onChange={event => this.checkValue(event, 2)}
                           name="numero" width="10" maxLength="2" className="boletaloto"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number3}
                           onChange={event => this.checkValue(event, 3)}
                           name="numero" width="10" maxLength="2" className="boletaloto"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number4}
                           onChange={event => this.checkValue(event, 4)}
                           name="numero" width="10" maxLength="2" className="boletaloto"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number5}
                           onChange={event => this.checkValue(event, 5)}
                           name="numero" width="10" maxLength="2" className="boletaloto"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number6}
                           onChange={event => this.checkValue(event, 6)}
                           name="numero" width="10" maxLength="2" className="boletaloto"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number7}
                           onChange={event => this.checkValue(event, 7)}
                           name="numero" width="10" maxLength="2" className="boletaloto"/>
                </div>
                <div className="col s3">
                    <input required="required" placeholder="00" type='text'
                           value={this.state.number8}
                           onChange={event => this.checkValue(event, 8)}
                           name="numero" width="10" maxLength="2" className="boletaloto"/>
                </div>
            </div>

        );
    }

    renderDetail() {
        let formatter = new Intl.NumberFormat('de-DE', {
            minimumFractionDigits: 2,
        });
        let loto = this.props.loto;
        let primera_tradicional = loto.tradicional_numbers.map((tradicional, index) => {
            let className = `btn-floating_poceada loto_numero ${this.state.actives[index]}`;
            return <div className="col s4" key={index}>
                <p className={className}>
                    {tradicional}
                </p>
            </div>
        });
        let winners_primera_tradicional = loto.tradicional_prizes.map((prize, index) => {
            if (index >= 3) return null;
            return (
                <div key={index}>
                    <div className="col l4 s12 loto_ganadores">{prize.label}</div>
                    <div className="col l4 s6 loto_ganadores">
                        {prize.winners === '0' ? 'POZO VACANTE' : prize.winners === '1' ? prize.winners + 'GANADOR' : prize.winners + ' GANADORES'}
                    </div>
                    <div className="col l4 s6 loto_ganadores">Cada uno ${formatter.format(prize.prize)}</div>
                </div>
            );
        });
        let winners_primera_tradicional_second = loto.tradicional_prizes.map((prize, index) => {
            if (index < 3) return null;
            return (
                <div key={index}>
                    <div className="col s4 loto_ganadores blanc">{prize.label}</div>
                    <div className="col s4 loto_ganadores blanc">
                        {prize.winners === '0' ? 'POZO VACANTE' : prize.winners === '1' ? prize.winners + 'GANADOR' : prize.winners + ' GANADORES'}
                    </div>
                    <div className="col s4 loto_ganadores blanc">Cada uno ${formatter.format(prize.prize)}</div>
                </div>
            );
        });
        let desquite = loto.desquite_numbers.map((desquite, index) => {
            let className = `btn-floating_poceada loto_numero`;
            return <div className="col s4" key={index}>
                <p className={className}>
                    {desquite}
                </p>
            </div>
        });
        let winners_desquite = loto.desquite_prizes.map((prize, index) => {
            return (
                <div key={index}>
                    <div className="col l4 s12 loto_ganadores">{prize.label}</div>
                    <div className="col l4 s6 loto_ganadores">
                        {prize.winners === '0' ? 'POZO VACANTE' : prize.winners === '1' ? prize.winners + 'GANADOR' : prize.winners + ' GANADORES'}
                    </div>
                    <div className="col l4 s6 loto_ganadores">Cada uno ${formatter.format(prize.prize)}</div>
                </div>
            );
        });
        let sale_sale = loto.sale_sale_numbers.map((sale_sale, index) => {
            let className = `btn-floating_poceada loto_numero`;
            return <div className="col s4" key={index}>
                <p className={className}>
                    {sale_sale}
                </p>
            </div>
        });
        console.log(loto);
        let winners_sale_sale =
            <div>
                <div className="col l4 s12 loto_ganadores">Con 5 aciertos</div>
                <div className="col l4 s6 loto_ganadores">
                    {loto.sale_sale_prizes.winners === '0' ? 'POZO VACANTE' : loto.sale_sale_prizes.winners === '1' ? loto.sale_sale_prizes.winners + 'GANADOR' : loto.sale_sale_prizes.winners + ' GANADORES'}
                </div>
                <div className="col l4 s6 loto_ganadores">Cada uno
                    ${formatter.format(loto.sale_sale_prizes.prize)}
                </div>
            </div>;

        return (
            <div>
                <div className="loto_bg">
                    <div className="col s12">
                        <div>
                            <AdsAutomatico/>
                        </div>
                        <h1 className="q2">LOTO</h1>
                        <label>Mi jugada:</label>
                        {this.renderComprobation()}
                        <div className="row">
                            <div className="waves-effect waves-teal">
                                <button
                                    onClick={this.handleSubmit}
                                    className="waves-light btn-large">
                                    Controlar
                                </button>
                            </div>
                        </div>


                        <div className="col l10 s12">
                            <p className="loto_tit">TRADICIONAL PRIMER SORTEO</p>
                            <p className="titaci">
                                <span id="aciertos">{this.state.message}</span>
                            </p>
                            {primera_tradicional}
                        </div>
                        <div className="col l2 s12">
                            <p className="loto_tit">JACKPOT</p>
                            <p className="titaci"/>
                            <div className="col s6 l6">
                                <p className="btn-floating_poceada loto_numero">
                                    {this.props.loto.jackpot1_tradicional}
                                </p>
                            </div>
                            <div className="col s6 l6">
                                <p className="btn-floating_poceada loto_numero">
                                    {this.props.loto.jackpot2_tradicional}
                                </p>
                            </div>
                        </div>
                        <div>
                            {winners_primera_tradicional}
                        </div>

                        <div className="col l12 s12">
                            <p className="loto_tit">DESQUITE</p>
                            <p className="titaci"></p>
                            {desquite}
                        </div>
                        <div>
                            {winners_desquite}
                        </div>

                        <div className="col l12 s12">
                            <p className="loto_tit">SALE O SALE</p>
                            <p className="titaci"></p>
                            {sale_sale}
                        </div>
                        <div>
                            {winners_sale_sale}
                        </div>

                        <div className="col l7 s12">
                            <p className="loto_tit">Premios Tradicional</p>
                            <p className="titaci"></p>
                            {winners_primera_tradicional_second}
                        </div>


                        <div className="col l5 s12">
                            <p className="loto_tit">POZO ESTIMADO PRÓXIMA JUGADA</p>
                            <p className="titaci"></p>
                            <div className="col s12 loto_ganadores blanc prize_next_move_loto">
                                $ {formatter.format(this.props.loto.prize_next_move)}
                            </div>
                            <div className="col s12 loto_ganadores suerte">SUERTE!</div>
                        </div>
                    </div>
                </div>
                <div>
                    <AdsAutomatico/>
                </div>
            </div>

        );
    }

    render() {
        return this.renderDetail();
    };
}

export default LotoDetail;