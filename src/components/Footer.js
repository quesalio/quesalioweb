import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="page-footer">
                <div className="container">
                    <div className="row">
                        <div className="col l6 s12 center">
                            <h5 className="white-text">¿Qué Salió?</h5>
                            <p className="grey-text text-lighten-4">En caso de discrepancia entre nuestros datos y el
                                extracto
                                oficial
                                tendrá validez este último.
                                Los nombres de juegos y productos aquí mencionados pueden ser marcas registradas de sus
                                respectivos
                                propietarios.</p>
                        </div>
                        <div className="col l3 s12 center">
                            <a href="https://play.google.com/store/apps/details?id=com.resultados.quesalio">
                                <h5 className="white-text">¡Bajate la App!</h5>
                                <p><img src="img/promoappstore.png" alt=""/></p>
                            </a>
                        </div>
                        <div className="col l3 s12 center">
                            <h5 className="white-text">Juegos</h5>
                            <ul>
                                <li className="li-q"><a className="white-text" href="/">Quiniela</a></li>
                                <li className="li-q"><a className="white-text" href="/poceada">Poceada</a></li>
                                <li className="li-q"><a className="white-text" href="/plus">Poceada Plus</a></li>
                                <li className="li-q"><a className="white-text" href="/quini">Quini</a></li>
                                <li className="li-q"><a className="white-text" href="/loto">Loto</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="footer-copyright">
                    <div className="container center">© QueSalio.com
                        <span role="img" aria-label="liberty">&#x1F5FD;</span>
                    </div>
                </div>
            </footer>
        );
    };
}

export default Footer;