import React, {Component} from 'react';

class Horoscope extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sign: '',
            number_one: '',
            number_two: '',
            prediction: '',
            image: ''
        };
    }

    componentDidMount() {
        const URL_SITE = "https://quesalio.com/";
        fetch('https://quesalio.com/fetcher_horoscope.php?action=random')
            .then(results => {
                return results.json();
            }).then(data => {
            this.setState({
                sign: data.sign,
                number_one: data.number_one,
                number_two: data.number_two,
                prediction: data.prediction,
                image: URL_SITE + data.image
            })
        })
    }

    render() {
        return (
            <div className="col s12 m6 l4">
                <div className="z-depth-2 fondotablero bghor">
                    <p className="tit">
                        <img className="icontit" src="img/logo_horoscopo_50x50.png" alt=""/>HORÓSCOPO DE HOY
                    </p>
                    <div className="col s4 hor1">
                        <img width="20px" src={this.state.image} alt=""/>{this.state.sign}
                    </div>
                    <div className="col s4 m30"><span className="nrohoros">{this.state.number_one}</span></div>
                    <div className="col s4 m30"><span className="nrohoros">{this.state.number_two}</span></div>
                    <p>
                        {this.state.prediction}
                    </p>
                    <a className="waves-effect waves-light btn-large indigo darken-4"
                       href="/horoscopo">Ver Horóscopo
                    </a>
                </div>
            </div>
        );
    };
}

export default Horoscope;