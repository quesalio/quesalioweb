import React, {Component} from 'react';
import {Navbar, NavItem} from 'react-materialize';

class Navigation extends Component {
    render() {
        return (
            <header>
                <Navbar brand='¿Qué Salió?' right fixed={true} className="">
                    <NavItem href='/'>Quiniela</NavItem>
                    <NavItem href='/poceada'>Poceada</NavItem>
                    <NavItem href='/plus'>Poceada Plus</NavItem>
                    <NavItem href='/quini'>Quini</NavItem>
                    <NavItem href='/loto'>Loto</NavItem>
                    <NavItem href='/horoscopo'>Horoscopo</NavItem>
                </Navbar>
            </header>
        );
    };
}

export default Navigation;