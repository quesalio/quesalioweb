import React, {Component} from 'react';
import Navigation from "./Navigation";
import Footer from "./Footer";
import AdsAutomatico from "./AdsAutomatico";
import { initAnalytics } from 'react-with-analytics';
import {Constants} from ".././Constants";
import { trackPage} from 'react-with-analytics';
initAnalytics(Constants.ANALYTICS_ID);

class HoroscopeList extends Component {
    render() {
        trackPage('/horoscope');
        return (
            <div>
                <Navigation/>
                <div className="container">
                    <div className="row">
                        <div id="pozos" className="section">
                            <div className="col s12 violeta2">
                                <div className="z-depth-2 center">
                                    <img className="bola2" src="../img/logo_horoscopo1.png" alt=""/>
                                    <p className="classhor">HORÓSCOPO</p>
                                    <p className="classstar">TU ESTRELLA</p>
                                    <div className="col s12 bghoroscopo">
                                        <div className="col s6 l6">
                                            <a href="/aries">
                                                <img width="90px" src="../img/horoscopo_img/aries.png" alt=""/>
                                                <p className="titlehoroscopo">ARIES</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/tauro">
                                                <img width="90px" src="../img/horoscopo_img/tauro.png" alt=""/>
                                                <p className="titlehoroscopo">TAURO</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/geminis">
                                                <img width="90px" src="../img/horoscopo_img/geminis.png" alt=""/>
                                                <p className="titlehoroscopo">GEMINIS</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/cancer">
                                                <img width="90px" src="../img/horoscopo_img/cancer.png" alt=""/>
                                                <p className="titlehoroscopo">CANCER</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/leo">
                                                <img width="90px" src="../img/horoscopo_img/leo.png" alt=""/>
                                                <p className="titlehoroscopo">LEO</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/virgo">
                                                <img width="90px" src="../img/horoscopo_img/virgo.png" alt=""/>
                                                <p className="titlehoroscopo">VIRGO</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/libra">
                                                <img width="90px" src="../img/horoscopo_img/libra.png" alt=""/>
                                                <p className="titlehoroscopo">LIBRA</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/escorpio">
                                                <img width="90px" src="../img/horoscopo_img/escorpio.png" alt=""/>
                                                <p className="titlehoroscopo">ESCORPIO</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/sagitario">
                                                <img width="90px" src="../img/horoscopo_img/sagitario.png" alt=""/>
                                                <p className="titlehoroscopo">SAGITARIO</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/capricornio">
                                                <img width="90px" src="../img/horoscopo_img/capricornio.png" alt=""/>
                                                <p className="titlehoroscopo">CAPRICORNIO</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="/acuario">
                                                <img width="90px" src="../img/horoscopo_img/acuario.png"
                                                     alt=""/>
                                                <p className="titlehoroscopo">ACUARIO</p>
                                            </a>
                                        </div>
                                        <div className="col s6 l6">
                                            <a href="piscis">
                                                <img width="90px" src="../img/horoscopo_img/piscis.png" alt=""/>
                                                <p className="titlehoroscopo">PISCIS</p>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="col s12 bghoroscopo">
                                        <p className="b1">¡Bajate la APP <img
                                            src="../img/horoscopo_img/notificaciones.png" width="32" alt=""/>!
                                        </p>
                                        <p className="b2">y recibí tu Horóscopo Diario!</p>
                                        <a href="https://play.google.com/store/apps/details?id=socialgold.horoscopotuestrella">
                                            <img src="../img/horoscopo_img/promo_horoscopo10.png" alt=""/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <AdsAutomatico/>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default HoroscopeList;