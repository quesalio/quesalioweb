import React, {Component} from 'react';
import Countdown from 'react-cntdwn';
import moment from 'moment';


class CountDown extends Component {
    renderSunday() {
        return (
            <div>
                <p className="tit">¡Quedate cerca!</p>
                <a href="/quini">
                    <div className="col s4 waves-effect waves-teal quiniBackground">
                        <p className="quinijuego">QUINI</p>
                        <p className="horajuego">21:15Hs</p>
                    </div>
                </a>
                <a href="/loto">
                    <div className="col s4 waves-effect waves-teal">
                        <p className="lotojuego">LOTO</p><p className="horajuego">22:00Hs</p>
                    </div>
                </a>
                <a className="waves-effect waves-light btn-large luck">Suerte</a>
            </div>
        );
    }

    renderMessage(draw, date) {
        switch (draw) {
            default:
            case 'primera':
                switch (date.day()) {
                    default:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return (
                            <div>
                                <p className="tit">LA PRIMERA 11:30Hs
                                    <img className="icontit" src="/img/eldia.png" alt=""/>
                                </p>
                                <div>
                                    <div>
                                        <span className="relojcerca">¡Quedate cerca!</span>
                                        <p className="titulotablero">El próximo sorteo comienza en</p>
                                    </div>
                                </div>
                            </div>
                        );
                    case 0: {
                        return this.renderSunday();
                    }
                }
            case 'matutina':
                switch (date.day()) {
                    default:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return (
                            <div>
                                <p className="tit">MATUTINA 14:00Hs
                                    <img className="icontit" src="/img/eldia.png" alt=""/>
                                </p>
                                <div>
                                    <div>
                                        <span className="relojcerca">¡Quedate cerca!</span>
                                        <p className="titulotablero">El próximo sorteo comienza en</p>
                                    </div>
                                </div>
                            </div>
                        );
                    case 0: {
                        return this.renderSunday();
                    }
                }
            case 'vespertina':
                switch (date.day()) {
                    default:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return (
                            <div>
                                <p className="tit">VESPERTINA 17:30Hs
                                    <img className="icontit" src="/img/latarde.png" alt=""/>
                                </p>
                                <div>
                                    <div>
                                        <span className="relojcerca">¡Quedate cerca!</span>
                                    </div>
                                </div>
                            </div>
                        );
                    case 0: {
                        return this.renderSunday();
                    }
                }
            case 'nocturna':
                switch (date.day()) {
                    default:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return (
                            <div>
                                <p className="tit">NOCTURNA 21:00Hs
                                    <img className="icontit" src="/img/lanoche.png" alt=""/>
                                </p>
                                <div>
                                    <div>
                                        <span className="relojcerca">¡Quedate cerca!</span>
                                    </div>
                                </div>
                            </div>
                        );
                    case 0: {
                        return this.renderSunday();
                    }
                }
        }
    }

    render() {
        let now = moment();
        let end = moment().endOf('day');
        let primera = moment().hour(11).minute(30).second(0).millisecond(0);
        let matutina = moment().hour(14).minute(0).second(0).millisecond(0);
        let vespertina = moment().hour(17).minute(30).second(0).millisecond(0);
        let nocturna = moment().hour(21).minute(0).second(0).millisecond(0);

        let dif = primera.diff(now);
        if (dif > 0) {
            return (
                <div>
                    {this.renderMessage('primera', primera)}
                    <span id="clock">
                        <Countdown
                            targetDate={primera.toDate()}
                            interval={1000}
                            timeSeparator={':'}
                            leadingZero
                            format={{hour: 'hh', minute: 'mm', second: 'ss'}}/>
                    </span>
                    <p>No apuestes sin antes ver los datos para hoy!</p>
                    <a className="waves-effect waves-light btn-large luck">Suerte</a>
                </div>
            );
        }
        dif = matutina.diff(now);
        if (dif > 0) {
            return (
                <div>
                    {this.renderMessage('matutina', matutina)}
                    <span id="clock">
                        <Countdown
                            targetDate={matutina.toDate()}
                            interval={1000}
                            timeSeparator={':'}
                            leadingZero
                            format={{hour: 'hh', minute: 'mm', second: 'ss'}}/>
                    </span>
                    <p>No apuestes sin antes ver los datos para hoy!</p>
                    <a className="waves-effect waves-light btn-large luck">Suerte</a>
                </div>
            );
        }
        dif = vespertina.diff(now);
        if (dif > 0) {
            return (
                <div>
                    {this.renderMessage('vespertina', vespertina)}
                    <span id="clock">
                        <Countdown
                            targetDate={vespertina.toDate()}
                            interval={1000}
                            timeSeparator={':'}
                            leadingZero
                            format={{hour: 'hh', minute: 'mm', second: 'ss'}}/>
                    </span>
                    <p>No apuestes sin antes ver los datos para hoy!</p>
                    <a className="waves-effect waves-light btn-large luck">Suerte</a>
                </div>
            );
        }
        dif = nocturna.diff(now);
        if (dif > 0) {
            return (
                <div>
                    {this.renderMessage('nocturna', nocturna)}
                    <span id="clock">
                        <Countdown
                            targetDate={nocturna.toDate()}
                            interval={1000}
                            timeSeparator={':'}
                            leadingZero
                            format={{hour: 'hh', minute: 'mm', second: 'ss'}}/>
                    </span>
                    <p>No apuestes sin antes ver los datos para hoy!</p>
                    <a className="waves-effect waves-light btn-large luck">Suerte</a>
                </div>
            );
        }
        dif = end.diff(now);
        primera = moment().add(1, 'days').hour(11).minute(30).second(0).millisecond(0);
        if (dif > 0) {
            return (
                <div>
                    {this.renderMessage('primera', primera)}
                    <span id="clock">
                        <Countdown
                            targetDate={primera.toDate()}
                            interval={1000}
                            timeSeparator={':'}
                            leadingZero
                            format={{hour: 'hh', minute: 'mm', second: 'ss'}}/>
                        </span>
                    <a className="waves-effect waves-light btn-large luck">Suerte</a>
                </div>
            );
        }
    };
}

export default CountDown;