import React, {Component} from 'react';
import './App.css';
import ApolloClient from 'apollo-boost';
import {ApolloProvider} from 'react-apollo';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Main from './Main';
import Poceada from './components/Poceada';
import PoceadaPlus from "./components/PoceadaPlus";
import Quini from "./components/Quini";
import Loto from "./components/Loto";
import HoroscopeList from "./components/HoroscopeList";
import HoroscopeSign from "./components/HoroscopeSign";

const client = new ApolloClient({
    //uri: 'https://production.quesalio.com:4007/gql'
    uri: 'https://production.quesalio.com/graphql'
    //uri: 'http://localhost:4000/graphql'
});

class App extends Component {
    render() {
        return (
            <ApolloProvider client={client}>
                <Router>
                    <div>
                        <Route exact path="/" component={Main}/>
                        <Route path="/poceada" component={Poceada}/>
                        <Route path="/plus" component={PoceadaPlus}/>
                        <Route path="/quini" component={Quini}/>
                        <Route path="/loto" component={Loto}/>
                        <Route path="/horoscopo" component={HoroscopeList}/>
                        <Route path="/aries" render={() => <HoroscopeSign sign='aries'/>}/>
                        <Route path="/tauro" render={() => <HoroscopeSign sign='tauro'/>}/>
                        <Route path="/geminis" render={() => <HoroscopeSign sign='gemini'/>}/>
                        <Route path="/cancer" render={() => <HoroscopeSign sign='cancer'/>}/>
                        <Route path="/leo" render={() => <HoroscopeSign sign='leo'/>}/>
                        <Route path="/virgo" render={() => <HoroscopeSign sign='virgo'/>}/>
                        <Route path="/libra" render={() => <HoroscopeSign sign='libra'/>}/>
                        <Route path="/escorpio" render={() => <HoroscopeSign sign='escorpio'/>}/>
                        <Route path="/sagitario" render={() => <HoroscopeSign sign='sagitario'/>}/>
                        <Route path="/capricornio" render={() => <HoroscopeSign sign='capricornio'/>}/>
                        <Route path="/acuario" render={() => <HoroscopeSign sign='acuario'/>}/>
                        <Route path="/piscis" render={() => <HoroscopeSign sign='piscis'/>}/>
                    </div>
                </Router>
            </ApolloProvider>
        );
    }
}

export default App;
