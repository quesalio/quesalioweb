import React, {Component} from 'react';
import EphemeridesList from './components/EphemeridesList';
import WeeklyList from './components/WeeklyList';
import DollarList from './components/DollarList';
import DrawList from './components/DrawList';
import Greeting from './components/Greeting';
import CountDown from './components/CountDown';
import Navigation from './components/Navigation'
import Footer from './components/Footer';
import Horoscope from "./components/Horoscope";
import AdsAutomatico from "./components/AdsAutomatico";
import { initAnalytics } from 'react-with-analytics';
import {Constants} from "./Constants";
import { trackPage} from 'react-with-analytics';
initAnalytics(Constants.ANALYTICS_ID);


class Main extends Component {
    render() {
        trackPage('/home');
        return (
            <div>
                <Navigation/>
                <div className="container">
                    <div className="row">
                        <div className="col s12">
                            <Greeting/>
                        </div>
                        <div className="row">
                            <DrawList/>
                        </div>
                        <div className="col s12">
                            <div id="pozos">
                                <div className="row center">
                                    <EphemeridesList/>
                                    <div className="col s12 m6 l4">
                                        <div className="z-depth-2 fondotablero esquina">
                                            <CountDown/>
                                        </div>
                                    </div>
                                    <div className="col s12 m6 l4">
                                        <div className="col s12 m12 l4 center pad0">
                                            <AdsAutomatico/>
                                        </div>
                                    </div>

                                    <Horoscope/>
                                </div>
                            </div>
                            <WeeklyList/>
                            <DollarList/>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Main;
